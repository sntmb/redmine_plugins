# rewrite view issues/index.html.erb

Redmine::Plugin.register :extra_queries do
  name 'Extra Queries plugin'
  author 'Kovalevsky Vasil'
  description 'This is a plugin for Redmine'
  version '2.3.7'
  url 'http://rmplus.pro/redmine/plugins/extra_queries'
  author_url 'http://rmplus.pro/'

  project_module :extra_queries do
    permission :eq_manage_query_categories, query_categories: [:index, :new, :create, :update, :edit, :create_of_query, :move_position, :destroy, :find_query_category]
  end

  requires_redmine '3.4.1'

  settings partial: 'extra_queries/settings', default: { 'custom_query_sidebar_enabled' => false }
end

Rails.application.config.to_prepare do
  load 'extra_queries/loader.rb'
end

Rails.application.config.after_initialize do
  ExtraQueries::LatePatches.load_all_dependencies
  ActionDispatch::Reloader.to_prepare do
    ExtraQueries::LatePatches.load_all_dependencies
  end

  plugins = { a_common_libs: '2.5.3', global_roles: '2.2.4' }
  plugin = Redmine::Plugin.find(:extra_queries)
  plugins.each do |k,v|
    begin
      plugin.requires_redmine_plugin(k, v)
    rescue Redmine::PluginNotFound => ex
      raise(Redmine::PluginNotFound, "Plugin requires #{k} not found")
    end
  end
end