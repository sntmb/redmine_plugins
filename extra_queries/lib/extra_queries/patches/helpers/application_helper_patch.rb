module ExtraQueries::Patches::Helpers
  module ApplicationHelperPatch
    def self.included(base)
      base.include InstanceMethods

      base.class_eval do
        alias_method_chain :format_object, :eq
      end
    end

    module InstanceMethods
      def format_object_with_eq(object, html=true, &block)
        if object.is_a?(Issue) && object['eq_p_id'].present?
          txt = "##{object.id.to_s}"
          if Setting.plugin_extra_queries['eq_sub_issues_extra_subject']
            txt = "#{txt}: #{object.subject}"
          end
          txt
        else
          format_object_without_eq(object, html, &block)
        end
      end
    end
  end
end