module ExtraQueries::Patches::Helpers
  module QueriesHelperPatch
    def self.included(base)
      base.send(:include, InstanceMethods)
      base.class_eval do
        alias_method_chain :column_value, :eq
        alias_method_chain :csv_value, :eq
        alias_method_chain :render_sidebar_queries, :eq
        alias_method_chain :sidebar_queries, :eq
      end
    end

    module InstanceMethods
      def column_value_with_eq(column, issue, value)
        if column && column.name == :category && value && @project && Setting.plugin_extra_queries['custom_query_sidebar_enabled']
          link_to(value.name, { controller: :issues, action: :index, project_id: @project, f: [:category_id, :status_id], op: { category_id: '=', status_id: 'o' }, v: { category_id: [value.id] }, set_filter: 1 })
        elsif column && column.name == :eq_sub_issues
          return '' if issue.leaf? || value.blank?
          txt = "##{value.id.to_s}"
          if Setting.plugin_extra_queries['eq_sub_issues_extra_subject']
            txt = "#{txt}: #{value.subject}"
          end
          link_to(txt, issue_path(value), class: value.css_classes).html_safe
        else
          column_value_without_eq(column, issue, value)
        end
      end

      def csv_value_with_eq(column, object, value)
        if value.present? && column.name == :eq_sub_issues
          txt = "##{value.id.to_s}"
          if Setting.plugin_extra_queries['eq_sub_issues_extra_subject']
            txt = "#{txt}: #{value.subject}"
          end
          txt
        else
          csv_value_without_eq(column, object, value)
        end
      end

      def render_sidebar_queries_with_eq(klass, project)
        if Setting.plugin_extra_queries['custom_query_sidebar_enabled']
          out = ''
          q = eq_render_sidebar_pinned_queries(klass, project)
          out << '<br>'
          out << "<div id='eq-sidebar-pinned-queries' class='eq-sidebar-queries' style='#{q.blank? ? 'display: none;' : ''}' data-url='#{url_for({ controller: :extra_queries, action: :pinned_queries_order, type: params[:controller] })}'>"
          out << q
          out << '</div>'

          out << "<fieldset class='eq-sidebar-queries eq-sidebar-taggable-fieldset#{session[:eq_queries_expanded] ? ' expanded' : ''}'>"
          out << '<legend>'
          out << '<span>'
          out << '<i class="fa fa-refresh fa-spin"></i>'
          out << link_to(l(:label_query_plural), { controller: :extra_queries, action: :query_group, query_group: 'queries', project_id: project.try(:identifier), type: params[:controller] }, class: 'in_link')
          out << '</span>'
          out << '</legend>'
          out << eq_render_sidebar_queries(klass, project) if session[:eq_queries_expanded]
          out << '</fieldset>'

          if klass == IssueQuery && project
            cat = eq_render_categories_links(klass, project)
            if cat.present?
              out << "<fieldset class='eq-sidebar-queries eq-sidebar-taggable-fieldset#{session[:eq_queries_by_category_expanded] ? ' expanded' : ''}'>"
              out << '<legend>'
              out << '<span>'
              out << '<i class="fa fa-refresh fa-spin"></i>'
              out << link_to(l(:eq_label_categories), { controller: :extra_queries, action: :query_group, query_group: 'categories', project_id: project.identifier, type: params[:controller] }, class: 'in_link')
              out << '</span>'
              out << '</legend>'
              out << cat if session[:eq_queries_by_category_expanded]
              out << '</fieldset>'
            end
          end

          out << "<div id='eq-sidebar-system-setting'>"
          if User.current.global_permission_to?(:eq_manage_query_categories)
            out << "#{link_to ('<i class="fa fa-cog">' + content_tag('span',l(:eq_manage_query_categories), style: "margin-left: 5px;") + '</i>').html_safe, {controller: 'query_categories', action: 'index'}, class: 'no_line'}"
          end
          out << '</div>'
          out.html_safe
        else
          render_sidebar_queries_without_eq(klass, project)
        end
      end

      def sidebar_queries_with_eq(klass, project)
        if Setting.plugin_extra_queries['custom_query_sidebar_enabled']
          unless @sidebar_queries
            @sidebar_queries = klass.visible
                                    .sorted
                                    .where(project.nil? ? ["project_id IS NULL"] : ["project_id IS NULL OR project_id = ?", project.id])
                                    .where(project.nil? ? "" : ["NOT EXISTS(SELECT 1 FROM eq_queries_hide_in_projects eq_hp WHERE eq_hp.query_id = #{Query.table_name}.id AND (eq_hp.project_id = ? OR (#{IssueQuery.table_name}.sub_project = ? AND eq_hp.project_id IN (#{(project.ancestors.map(&:id) + [0]).join(', ')}) ))))", project.id, true])
                                    .where(project.nil? ? "" : ["NOT EXISTS(SELECT 1 FROM eq_queries_show_in_projects eq_sp WHERE eq_sp.query_id = #{Query.table_name}.id) OR EXISTS(SELECT 1 FROM eq_queries_show_in_projects eq_sp WHERE eq_sp.query_id = #{Query.table_name}.id AND (eq_sp.project_id = ? OR ( #{IssueQuery.table_name}.sub_project = ? AND eq_sp.project_id IN (#{(project.ancestors.map(&:id) + [0]).join(', ')}) )))", project.id, true])
                                    .to_a
          end
          @sidebar_queries
        else
          sidebar_queries_without_eq(klass, project)
        end
      end
    end
  end
end