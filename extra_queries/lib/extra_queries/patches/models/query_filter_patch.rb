module ExtraQueries::Patches::Models
  module QueryFilterPatch
    def self.included(base)
      base.class_eval do
        attr_accessor :eq_filter, :eq_default
      end
    end
  end
end