module ExtraQueries::Patches::Models
  module QueryAssociationColumnPatch
    def value(object)
      obj = self.value_object(object)
      if obj.is_a?(IssueCategory)
        obj.name
      else
        obj
      end
    end
  end
end