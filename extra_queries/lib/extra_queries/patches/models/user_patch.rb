module ExtraQueries::Patches::Models
  module UserPatch
    def self.included(base)
      base.send :include, InstanceMethods
    end

    module InstanceMethods
      def eq_issues_count(view_context=nil, params=nil, session={})
        params ||= {}
        if params[:query_id].to_i > 0
          project_tp = params[:project_id] || 'all'
          @counts ||= {}
          @counts[project_tp] ||= {}

          if @counts[project_tp][params[:query_id].to_i].present? && @counts[project_tp][params[:query_id].to_i][:query].present?
            query = @counts[project_tp][params[:query_id].to_i][:query]
          else
            begin
              query = IssueQuery.find(params[:query_id])
              query.group_by = nil
              query.column_names = nil
              if params[:project_id].present?
                project = Project.find(params[:project_id])
                query.project = project
              end
              @counts[project_tp][query.id] ||= {}
              @counts[project_tp][query.id][:query] = query
            rescue ActiveRecord::RecordNotFound
              return 0
            end
          end

          scope = Issue.visible.joins(:status, :project)

          if Redmine::Plugin.installed?(:unread_issues)
            tp = (params[:only] || 'all').to_s
            if tp == 'unread'
              scope = scope.includes(:uis_user_read).where("#{IssueRead.table_name}.read_date is null")
            elsif tp == 'updated'
              scope = scope.includes(:uis_user_read).where("#{IssueRead.table_name}.read_date < #{Issue.table_name}.updated_on")
            elsif tp != 'all'
              return 0
            end
          else
            tp = 'all'
          end

          if @counts[project_tp][query.id][tp]
            @counts[project_tp][query.id][tp]
          else
            @counts[project_tp][query.id][tp] = scope.where(query.send(:statement)).pluck(:id).size
          end
        else
          0
        end
      end
    end
  end
end