module ExtraQueries::Patches::Models
  module QueryPatch
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        belongs_to :query_category, foreign_key: :category_id

        has_many :eq_pinned_queries, foreign_key: :query_id
        has_one :eq_pinned_query, -> { where("#{EqPinnedQuery.table_name}.user_id is null or #{EqPinnedQuery.table_name}.user_id = #{User.current.id}").order("case when #{EqPinnedQuery.table_name}.user_id is null then 0 else #{EqPinnedQuery.table_name}.user_id end") }, class_name: 'EqPinnedQuery', foreign_key: :query_id, dependent: :destroy
        has_one :eq_admin_pinned_query, -> { where("#{EqPinnedQuery.table_name}.user_id is null") }, class_name: 'EqPinnedQuery', foreign_key: :query_id, dependent: :destroy
        has_one :eq_user_pinned_query, -> { where("#{EqPinnedQuery.table_name}.user_id = #{User.current.id}") }, class_name: 'EqPinnedQuery', foreign_key: :query_id, dependent: :destroy
        accepts_nested_attributes_for :eq_admin_pinned_query, :eq_user_pinned_query, allow_destroy: true

        has_and_belongs_to_many :eq_hide_in_projects, class_name: 'Project', join_table: "#{table_name_prefix}eq_queries_hide_in_projects#{table_name_suffix}", foreign_key: 'query_id'
        has_and_belongs_to_many :eq_show_in_projects, class_name: 'Project', join_table: "#{table_name_prefix}eq_queries_show_in_projects#{table_name_suffix}", foreign_key: 'query_id'


        scope :eq_sidebar_queries, lambda { |project|
          self.includes(:query_category)
              .preload(:eq_pinned_query)
              .preload(:eq_admin_pinned_query)
              .where(project ? ["#{Query.table_name}.project_id IS NULL OR #{Query.table_name}.project_id = ?", project.id] : ["#{Query.table_name}.project_id IS NULL"])
              .where(project ? ["NOT EXISTS(SELECT 1 FROM eq_queries_hide_in_projects eq_hp WHERE eq_hp.query_id = #{Query.table_name}.id AND (eq_hp.project_id = ? OR (#{Query.table_name}.sub_project = ? AND eq_hp.project_id IN (#{(project.ancestors.map(&:id) + [0]).join(', ')}) )))", project.id, true ]: '')
              .where(project ? ["NOT EXISTS(SELECT 1 FROM eq_queries_show_in_projects eq_sp WHERE eq_sp.query_id = #{Query.table_name}.id) OR EXISTS(SELECT 1 FROM eq_queries_show_in_projects eq_sp WHERE eq_sp.query_id = #{Query.table_name}.id AND (eq_sp.project_id = ? OR ( #{Query.table_name}.sub_project = ? AND eq_sp.project_id IN (#{(project.ancestors.map(&:id) + [0]).join(', ')}) )))", project.id, true] : '')
        }

        scope :eq_pinned, -> {
          self.joins("INNER JOIN
                      (
                        SELECT eq.query_id
                        FROM #{EqPinnedQuery.table_name} eq
                        WHERE eq.user_id is null or eq.user_id = #{User.current.id}
                        GROUP BY eq.query_id
                      ) eq_pinned on eq_pinned.query_id = #{Query.table_name}.id
                     ")
        }

        validate :eq_edit_global_from_project
        attr_accessor :eq_editable_by_was
        attr_reader :eq_current_project, :eq_project_was
        alias_method_chain :editable_by?, :eq
        alias_method_chain :build_from_params, :eq
      end
    end

    module InstanceMethods
      # to save applied filter (change selected filter, click apply to see result and then need to save result), by default rm create new Query instance, so there is no Save button
      def editable_by_with_eq?(user)
        return editable_by_without_eq?(user) if self.new_record? || self.eq_editable_by_was

        q = Query.where(id: self.id).first
        return editable_by_without_eq?(user) if q.blank?
        q.eq_editable_by_was = true
        q.editable_by?(user)
      end

      def build_from_params_with_eq(params)
        unless Setting.plugin_extra_queries['custom_query_page_enabled']
          return build_from_params_without_eq(params)
        end

        if params[:saved_query_id].to_i != 0 && self.new_record?
          query = Query.find(params[:saved_query_id].to_i)
          self.attributes = query.attributes.dup
          self.id = query.id
          @new_record = false
          @eq_reassign = true
        end
        res = build_from_params_without_eq(params)

        res.category_id = params[:query][:category_id] if params[:query].present? && params[:query].has_key?(:category_id)

        if res.project.nil?
          res.eq_hide_in_project_ids = params[:query][:hide_in_projects] if params[:query].present? && params[:query].has_key?(:hide_in_projects)
          res.eq_show_in_project_ids = params[:query][:show_in_projects] if params[:query].present? && params[:query].has_key?(:show_in_projects)
        else
          res.eq_hide_in_project_ids = nil
          res.eq_show_in_project_ids = nil
        end

        @eq_current_project = nil
        if params[:current_project_id].present?
          begin
            @eq_current_project = Project.find(params[:current_project_id])
          rescue ActiveRecord::RecordNotFound
            @eq_current_project = nil
          end
        end

        res
      end

      def eq_filters
        return @eq_filters if (@eq_filters)

        filter_fields = self.is_a?(IssueQuery) ? (Setting.plugin_extra_queries['default_filter_fields'] || []) : []
        filter_fields = self.available_filters.slice(*filter_fields)
        sort = {}
        # let s sort default filters
        filter_fields.sort { |(pk, pv), (k, v)| pv[:name] <=> v[:name] }.each do |it|
          sort[it[0]] = it[1]
        end
        filter_fields = sort

        # no sort, just like was added by user
        self.filters.each do |field, options|
          if (v = self.available_filters[field])
            v.eq_filter = options # filter { operator: '=', values: ['1'] }
            v.eq_default = filter_fields.has_key?(field)
            filter_fields[field] = v
          end
        end
        @eq_filters = filter_fields
      end

      def eq_available_operator_labels(type)
        available_operators = Query.operators_by_filter_type[type]
        if available_operators.present?
          available_operators = available_operators.inject([]) { |res, it| res << [l(*Query.operators[it.to_s]), it.to_s] if Query.operators[it.to_s].present?; res }
        end
        available_operators
      end

      def hide_in_projects
        self.eq_hide_in_project_ids
      end

      def hide_in_projects=(v)
        self.eq_hide_in_project_ids = v
      end

      def show_in_projects
        self.eq_show_in_project_ids
      end

      def show_in_projects=(v)
        self.eq_show_in_project_ids = v
      end

      def eq_pinned?
        self.eq_pinned_query.present?
      end

      def eq_admin_pinned?
        self.eq_admin_pinned_query.present?
      end

      def eq_is_for_all?
        if @is_for_all.nil? && !self.new_record?
          @is_for_all = self.project_id_was.nil? || self.project.nil?
        end
        @is_for_all
      end

      def eq_editable_by_global_status?
        self.new_record? || self.eq_current_project.blank? || self.project.present?
      end

      def project=(project)
        @eq_project_was ||= self.project
        super(project)
      end

      private

      def eq_edit_global_from_project
        unless self.eq_editable_by_global_status?
          self.errors.add(:base, l(:eq_error_global_query_editing_from_project))
        end
      end
    end
  end
end