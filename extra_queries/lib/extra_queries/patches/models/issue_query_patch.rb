module ExtraQueries::Patches::Models
  module IssueQueryPatch
    def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        attr_reader :eq_reassign

        alias_method_chain :author_values, :eq
        alias_method_chain :assigned_to_values, :eq
        alias_method_chain :issue_statuses_values, :eq
        alias_method_chain :initialize_available_filters, :eq
        alias_method_chain :sql_for_field, :eq
        alias_method_chain :available_columns, :eq
        alias_method_chain :issues, :eq

        self.operators_by_filter_type[:tree_list] = %w(= != ~~ !* *)

        self.operators['~~'] = :label_self_and_descendants
      end
    end

    module InstanceMethods
      def author_values_with_eq
        res = author_values_without_eq
        locked_users = self.acl_locked_users
        if locked_users.size > 0
          res |= ([[l(:acl_dismissed_users), '', { group_title: true }]] + locked_users.map { |u| [u.name, u.id] })
        end
        res
      end

      def assigned_to_values_with_eq
        res = assigned_to_values_without_eq
        locked_users = self.acl_locked_users
        if locked_users.size > 0
          res |= ([[l(:acl_dismissed_users), '', { group_title: true }]] + locked_users.map { |u| [u.name, u.id] })
        end
        res
      end

      def issue_statuses_values_with_eq
        issue_statuses_values_without_eq.sort_by {|m| m[0]}
      end

      def all_available_filters
        return @all_available_filters if (@all_available_filters)
        @all_available_filters = self.available_filters
        add_available_filter('project_id', type: :list, values: []) if (@all_available_filters['project_id'].nil?)
        add_available_filter('author_id', type: :list, values: []) if (@all_available_filters['author_id'].nil?)
        add_available_filter('assigned_to_id', type: :list_optional, values: []) if (@all_available_filters['assigned_to_id'].nil?)
        add_available_filter('member_of_group', type: :list_optional, values: []) if (@all_available_filters['member_of_group'].nil?)
        add_available_filter('assigned_to_role', type: :list_optional, values: []) if (@all_available_filters['assigned_to_role'].nil?)
        add_available_filter('fixed_version_id', type: :list_optional, values: []) if (@all_available_filters['fixed_version_id'].nil?)
        add_available_filter('category_id', type: :list_optional, values: []) if (@all_available_filters['category_id'].nil?)
        add_available_filter('is_private', type: :list, values: [[l(:general_text_yes), "1"], [l(:general_text_no), "0"]]) if (@all_available_filters['is_private'].nil?)
        add_available_filter('watcher_id', type: :list, values: [["<< #{l(:label_me)} >>", "me"]]) if (@all_available_filters['watcher_id'].nil?)
        add_available_filter('subproject_id', type: :list_subprojects, values: []) if (@all_available_filters['subproject_id'].nil?)

        Tracker.disabled_core_fields(trackers).each { |field| delete_available_filter(field) }

        @all_available_filters.each do |field, options|
          options[:name] ||= l(options[:label] || "field_#{field}".gsub(/_id$/, ''))
        end
        sort = {}
        @all_available_filters.sort { |(pk, pv), (k, v)| pv[:name] <=> v[:name] }.each do |it|
          sort[it[0]] = it[1]
        end

        return @all_available_filters = sort
      end

      def initialize_available_filters_with_eq
        initialize_available_filters_without_eq
        unless Setting.plugin_extra_queries['custom_query_page_enabled']
          add_available_filter 'relation_tracker_eq', type: :list, values: trackers.map { |s| [s.name, s.id.to_s] }, label: :eq_label_relation_tracker_filter_caption
        end
      end

      def sql_for_relation_tracker_eq_field(field, operator, value, options={})
        "(EXISTS(SELECT 1
                 FROM #{IssueRelation.table_name} eq_ir
                      INNER JOIN #{Issue.table_name} eq_if on eq_if.id = eq_ir.issue_from_id
                      INNER JOIN #{Issue.table_name} eq_it on eq_it.id = eq_ir.issue_to_id
                 WHERE (eq_ir.issue_from_id = #{Issue.table_name}.id and eq_it.tracker_id #{(operator == '=' ? '' : 'NOT')} in (#{((!value.is_a?(Array) ? [value] : value).map(&:to_i) + [0]).join(',')}))
                    or (eq_ir.issue_to_id = #{Issue.table_name}.id and eq_if.tracker_id #{(operator == '=' ? '' : 'NOT')} in (#{((!value.is_a?(Array) ? [value] : value).map(&:to_i) + [0]).join(',')}))
                 )
         )"
      end

      def sql_for_field_with_eq(field, operator, value, db_table, db_field, is_custom_filter=false)
        if !is_custom_filter || type_for(field) != :tree_list || operator != '~~'
          return sql_for_field_without_eq(field, operator, value, db_table, db_field, is_custom_filter)
        end

        filter = @available_filters[field]
        return nil unless filter

        link_table = filter[:field].format.target_class.table_name

        value = Array.wrap(value)
        items = filter[:field].format.target_class.where(id: value.map(&:to_i) + [0]).to_a
        if items.present?
          conditions = []
          items.each do |it|
            conditions << "tree_cf.lft >= #{it.lft} and tree_cf.rgt <= #{it.rgt}"
          end
          "#{db_table}.#{db_field} IN (SELECT tree_cf.id FROM #{link_table} tree_cf WHERE (#{conditions.join(') OR (')}))"
        else
          '1=0'
        end
      end

      def available_columns_with_eq
        return available_columns_without_eq if @available_columns
        available_columns_without_eq

        @available_columns << QueryColumn.new(:eq_sub_issues,
                                              caption: :label_subtask_plural,
                                              groupable: false,
                                              totalable: false)
      end

      def issues_with_eq(options={})
        i = issues_without_eq(options)

        if self.has_column?(:eq_sub_issues)
          Issue.load_eq_sub_issues(i)
        end

        i
      end
    end
  end
end