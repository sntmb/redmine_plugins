module ExtraQueries::Patches::Models
  module IssuePatch
    def self.included(base)
      base.extend ClassMethods
      base.class_eval do
        attr_accessor :eq_sub_issues
      end
    end

    module ClassMethods
      def load_eq_sub_issues(issues)
        return if issues.blank?

        hash = issues.inject({}) { |h, i| h[i.id] = i; h }
        Issue.visible
             .preload(:status, :priority, { project: :enabled_modules })
             .joins("INNER JOIN (SELECT i.id, i.root_id, i.lft, i.rgt FROM #{Issue.table_name} i WHERE i.id IN (#{hash.keys.join(', ')})) eq_i ON eq_i.root_id = #{Issue.table_name}.root_id and eq_i.lft < #{Issue.table_name}.lft and eq_i.rgt > #{Issue.table_name}.rgt")
             .select("#{Issue.table_name}.*,
                      eq_i.id as eq_p_id
                     ")
             .distinct
             .each do |i|

          (hash[i['eq_p_id']].eq_sub_issues ||= []) << i
        end
      end
    end
  end
end