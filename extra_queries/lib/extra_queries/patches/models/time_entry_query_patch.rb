module ExtraQueries::Patches::Models
  module TimeEntryQueryPatch
    def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        tweek_index = self.available_columns.find_index {|x| x.name == :tweek}
        if tweek_index
          self.available_columns[tweek_index] = QueryColumn.new(:tweek, :sortable => ["#{TimeEntry.table_name}.spent_on", "#{TimeEntry.table_name}.created_on"], :caption => :label_week)
        end

        alias_method_chain :initialize_available_filters, :eq
        alias_method_chain :available_columns, :eq
        alias_method_chain :joins_for_order_statement, :eq
      end
    end

    module InstanceMethods
      def initialize_available_filters_with_eq
        initialize_available_filters_without_eq
        add_available_filter 'issue.category_id',
                             type: :list,
                             values: lambda { (project.nil? ? IssueCategory.joins(:project) : project.issue_categories).map { |s| [s.name, s.id.to_s] } },
                             name: l(:label_attribute_of_issue, name: l(:field_category))
      end
      def available_columns_with_eq
        return @available_columns if @available_columns
        available_columns_without_eq
        @available_columns << QueryAssociationColumn.new(:issue, :category, sortable: "#{IssueCategory.table_name}.name", groupable: "#{IssueCategory.table_name}.name", caption: l(:label_attribute_of_issue, name: l(:field_category)))
        @available_columns
      end

      def sql_for_issue_category_id_field(field, operator, value)
        sql_for_field('category_id', operator, value, Issue.table_name, 'category_id')
      end

      def joins_for_order_statement_with_eq(order_options)
        joins = [joins_for_order_statement_without_eq(order_options)]

        if order_options
          if order_options.include?("#{IssueCategory.table_name}.name")
            joins << "LEFT JOIN #{IssueCategory.table_name} ON #{IssueCategory.table_name}.id = #{Issue.table_name}.category_id"
          end
        end

        joins.compact!
        joins.any? ? joins.join(' ') : nil
      end
    end
  end
end