module ExtraQueries::Patches::Redmine::FieldFormat
  module UserFormatPatch
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        alias_method_chain :query_filter_values, :equ
      end
    end

    module InstanceMethods
      def query_filter_values_with_equ(custom_field, query)
        if Setting.plugin_extra_queries['custom_query_page_enabled'] && custom_field.format.respond_to?(:ajax_supported) && custom_field.format.ajax_supported && custom_field.ajaxable
          Redmine::FieldFormat::RecordList.instance_method(:query_filter_values).bind(self).call(custom_field, query)
        else
          query_filter_values_without_equ(custom_field, query)
        end
      end

      def query_filter_values_for_ajaxable(query, custom_field, object=nil, like=nil, current_vals=nil)
        eq_user_values(custom_field, query, like, current_vals)
      end

      def query_default_values(query, like=nil)
        p = query.instance_variable_get '@principal'
        lu = query.instance_variable_get '@acl_locked_users'
        query.instance_variable_set '@principal', []
        query.instance_variable_set '@acl_locked_users', []

        default_vals = query.author_values

        query.instance_variable_set '@principal', p
        query.instance_variable_set '@acl_locked_users', lu

        if like.present?
          default_vals.select { |it| it[0].to_s.mb_chars.downcase.include?(like.to_s.mb_chars.downcase) }
        else
          default_vals
        end
      end

      def eq_user_values(custom_field, query, like=nil, vals=nil)
        if custom_field.user_role.is_a?(Array) && (role_ids = custom_field.user_role.map(&:to_s).reject(&:blank?).map(&:to_i)).any?
          users = query.acl_principals_scope.where("#{User.table_name}.type = 'User'").where("#{Member.table_name}.id IN (SELECT DISTINCT member_id FROM #{MemberRole.table_name} WHERE role_id IN (?))", role_ids)
        elsif vals.present? || like.present?
          users = query.acl_principals_scope.where("#{User.table_name}.type = 'User'")
        else
          users = query.acl_users
        end

        if vals.present?
          users = users.where("#{User.table_name}.id in (?)", vals + [0])
        elsif like.present?
          users = users.like(like)
        end
        users_active = []
        users_locked = []

        users.each do |u|
          users_active << [u.name, u.id.to_s] if u.active?
          users_locked << [u.name, u.id.to_s] if u.locked?
        end
        values = users_active

        if users_locked.size > 0
          if values.size > 0
            values += [[l(:acl_dismissed_users), '', { group_title: true }]] + users_locked
          else
            values += users_locked
          end
        end

        values
      end
    end
  end
end