module ExtraQueries::Patches::Redmine::FieldFormat
  module RecordListPatch
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        alias_method_chain :query_filter_values, :eqr
      end
    end

    module InstanceMethods
      def query_filter_values_with_eqr(custom_field, query)
        if Setting.plugin_extra_queries['custom_query_page_enabled'] && custom_field.format.respond_to?(:ajax_supported) && custom_field.format.ajax_supported && custom_field.ajaxable
          like = query.acl_ajax_like
          field = "cf_#{custom_field.id}"
          filters = query.filters || {}
          if like.blank? && filters[field].is_a?(Hash) && filters[field][:values].is_a?(Array)
            current_vals = filters[field][:values].map(&:to_s)
          else
            current_vals = []
          end

          return query_default_values(query, like) if like.blank? && current_vals.blank?

          res = query_filter_values_for_ajaxable(query, custom_field, query.project || query.all_projects, like, current_vals)
          if current_vals.present?
            query_default_values(query) + res
          else
            query_default_values(query, like) + res
          end
        else
          query_filter_values_without_eqr(custom_field, query)
        end
      end

      def query_default_values(query, like=nil)
        []
      end
    end
  end
end