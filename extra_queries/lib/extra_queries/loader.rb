require 'extra_queries/hooks/view_hooks'
require_dependency 'extra_queries/helpers/issues_extend_helper'

require 'extra_queries/patches'
ExtraQueries::Patches.load_all_dependencies


Acl::Settings.append_setting('enable_select2_lib', :extra_queries)
Acl::Settings.append_setting('enable_bootstrap_lib', :extra_queries)
Acl::Settings.append_setting('enable_javascript_patches', :extra_queries)
Acl::Settings.append_setting('enable_modal_windows', :extra_queries)
Acl::Settings.append_setting('enable_font_awesome', :extra_queries)
Acl::Settings.append_setting('enable_ajax_counters', :extra_queries)