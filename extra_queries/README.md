## ExtraQueries

#### Plugin for Redmine

Plugin that greatly improves usablity of Redmine.
It implements a lof of things that made Redmine interface user-friendly.


#### Copyright
Copyright (c) 2011-2018 Vladimir Pitin, Danil Kukhlevskiy.

Another plugins of our team you can see on site http://rmplus.pro

Changelog:
2.3.7
* Added: support a_common_libs settings
* Fixed: minor bugs
2.3.6
* Added: subissues field to issues queries
* Fixed: patches order
* Fixed: delete query by IE
2.3.5
* Fixed: css style
2.3.4
* Updated: move manage category down in the sidebar
2.3.3
* Changed: rename labels
2.3.2
* Added: blocked to save global queries in project context
* Added: filter by tree
* Added: split counters by issue query to unread\updated\total if exists unread_issues plugin
* Added: sorting of statutes in alphabetical order
* Added: support redmine 3.4
* Removed: support redmine below 3.4
* Fixed: sorting in queries for the time spent
* Fixed: ajax loading
2.2.9
* Fixed: fix minor bugs
2.2.8
* Added: show cross-project query in certain projects
2.2.7
* Fixed: ajaxable loading
2.2.6
* Added: support new ajax_counters
* Added: flexible integration
2.2.5
* fixed mmp settings
* deleted queries by role and field
2.2.4
* fixed: css for selected fields
2.2.3
* fixed: special query filters by date
* fixed: queries in time entries
2.2.2
* fixed: redmine auto check version for plugin
* removed: global_roles plugin dependency
2.2.1
* fixed: right to manage global queries
* fixed: apply empty filter raise 500 error
* fixed: stack level too deep when custom view of filters disabled

2.2.0
* async custom fields support

2.1.0
* Added: new filter type: tree_list

2.0.3
* Redmine 3.2 support

2.0.2
* fix relative links

2.0.1
* fix saving roles for query
* added ability for custom render filter item

2.0.0
* support RM3

1.0.2
* added filter by tracker of relation issues
* added filter by ldap
* some improvements

1.0.1
* fixed bugs, added new
* added for timelog queries
* added blocked users to filters
* added full list of users to global (not project) queries