Redmine::Plugin.register :global_roles do
  name 'Global Roles plugin'
  author 'Danil Kukhlevskiy'
  description 'This is a plugin for Redmine which provides usage of standart Roles privelegies for non-project objects.'
  version '2.2.4'
  url 'http://rmplus.pro/redmine/plugins/global_roles'
  author_url 'http://rmplus.pro'
end

Rails.application.config.before_initialize do
  unless Redmine::AccessControl::Permission.included_modules.include?(GlobalRoles::AccessControlPatch)
    Redmine::AccessControl::Permission.send(:include, GlobalRoles::AccessControlPatch)
  end
end

Rails.application.config.to_prepare do
  require 'global_roles/view_hooks'

  unless User.included_modules.include?(GlobalRoles::UserPatch)
    User.send(:include, GlobalRoles::UserPatch)
  end
  unless Group.included_modules.include?(GlobalRoles::GroupPatch)
    Group.send(:include, GlobalRoles::GroupPatch)
  end
  unless UsersHelper.included_modules.include?(GlobalRoles::UsersHelperPatch)
    UsersHelper.send(:include, GlobalRoles::UsersHelperPatch)
  end
  unless UsersController.included_modules.include?(GlobalRoles::UsersControllerPatch)
    UsersController.send(:include, GlobalRoles::UsersControllerPatch)
  end
  unless GroupsHelper.included_modules.include?(GlobalRoles::GroupsHelperPatch)
    GroupsHelper.send(:include, GlobalRoles::GroupsHelperPatch)
  end
  unless GroupsController.included_modules.include?(GlobalRoles::GroupsControllerPatch)
    GroupsController.send(:include, GlobalRoles::GroupsControllerPatch)
  end
  unless Role.included_modules.include?(GlobalRoles::RolePatch)
    Role.send(:include, GlobalRoles::RolePatch)
  end
  unless RolesController.included_modules.include?(GlobalRoles::RolesControllerPatch)
    RolesController.send(:include, GlobalRoles::RolesControllerPatch)
  end
  unless ApplicationController.included_modules.include?(GlobalRoles::ApplicationControllerPatch)
    ApplicationController.send(:include, GlobalRoles::ApplicationControllerPatch)
  end
  unless MemberRole.included_modules.include?(GlobalRoles::MemberRolePatch)
    MemberRole.send(:include, GlobalRoles::MemberRolePatch)
  end

  Acl::Settings.append_setting('enable_javascript_patches', :global_roles)
end

Rails.application.config.after_initialize do
  plugins = { a_common_libs: '2.5.3' }
  plugin = Redmine::Plugin.find(:global_roles)
  plugins.each do |k,v|
    begin
      plugin.requires_redmine_plugin(k, v)
    rescue Redmine::PluginNotFound => ex
      raise(Redmine::PluginNotFound, "Plugin requires #{k} not found")
    end
  end
end