module GlobalRoles
  module AccessControlPatch
    def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        alias_method_chain :initialize, :global_roles
      end
    end

    module InstanceMethods
      def initialize_with_global_roles(name, hash, options)
        @description = options[:description]
        initialize_without_global_roles(name, hash, options)
      end

      def description
        if @description.blank?
          return I18n.t("permission_desc_#{self.project_module}_#{self.name}", default: '') if self.project_module.present?
          return I18n.t("permission_desc_#{self.name}", default: '')
        end

        @description.is_a?(Proc) ? @description.call : @description
      end

      def description=(new_value)
        @description = new_value
      end
    end
  end
end