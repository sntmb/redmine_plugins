class AddGlobalRoleIndexes < ActiveRecord::Migration
  def change
    add_index :global_roles, [:user_id], unique: false
    add_index :global_roles, [:role_id], unique: false
    add_index :global_roles, [:user_id, :role_id], unique: false
  end
end