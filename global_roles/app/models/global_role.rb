class GlobalRole < ActiveRecord::Base
  belongs_to :role
  belongs_to :principal, foreign_key: :user_id
  validates_uniqueness_of :user_id, scope: :role_id

  attr_protected :id
end
