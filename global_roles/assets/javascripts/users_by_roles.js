// Module for global roles plugin
RMPlus.GlobalRoles = (function(my){
  var my = my || {};

  // variable to hold jQuery object of the form for editing projects by user
  my.$projectsForm = {};

  my.FetchUsers = function(element, data){
    $.ajax({url: $(element).attr('data-url'),
           type: 'get',
           data: data,
           success: function(data){},
           beforeSend: function(){ $(element).addClass('ajax-loading'); },
           complete: function(){ $(element).removeClass('ajax-loading'); }
    });
  }

  return my;
})(RMPlus.GlobalRoles || {});



$(document).ready(function () {

  $(document.body).on('click', '.glr-projects-edit, .projects-form-cancel', function () {
    $('.principal-projects').hide();
    $('.glr-projects-names').show();
  });

  // cuts tabs from the bottom of the page and puts them in their proper place
  var t = $('#roles_tabs');
  $('#roles_tabs').remove();
  $('div#content h2').after(t);
  $('#roles_tabs').removeClass('I');

  // cuts rendered 'roles/edit' view and puts it in the tab
  var edit_partial = '';
  if ($('form.edit_role').length == 1) {
    edit_partial = $('form.edit_role')[0].outerHTML;
  }
  $('form.edit_role').remove();
  $('div#tab-content-roles_edit').html(edit_partial);

  // caches form in the variable for future cloning as needed
  RMPlus.GlobalRoles.$projectsForm = $('#member-projects-form');

  // We have to attach handler to the cancel buttons of the forms to the body of the document
  // since forms are cloned interactively after page load
  $('body').on("click", function (event){
    if (event.target.id === "projects-form-cancel"){
      var principal_id = event.target.getAttribute('data-principal-id');
      $('#principal-'+principal_id+'-project-links').show();
      $('#principal-'+principal_id+'-projects form').hide();
    }
  });

  // checks if user selected project
  $('#add_user_to_role').on("click", function(event){
    var value = $('#project_id').val();
    if (value === '') {
      event.preventDefault();
      $('.glr-no-project-selected').show();
    }
  });

  $('body').on("change keyup input", '#filter_users', RMPlus.Utils.debounce(300, function(event){
    $this = $(this);
    var value = $this.val().trim();
    var old_value = $this.attr('data-value-was');
    if (value == ''){
      jQuery('#users-list label.one-name').show();
    }
    else{
      jQuery('#users-list label.one-name').hide();
      jQuery('#users-list label.one-name:Contains('+value+')').show();
    }

    if (value != old_value){
      $this.attr('data-value-was', value);
      data = { project_id: $('#project_id').val(), user_name: value };
      RMPlus.GlobalRoles.FetchUsers(this, data);
    }
  }));

  $('body').on("change keyup input", '#filter_users_global_roles', RMPlus.Utils.debounce(300, function(){
    var $this = $(this);
    var value = $this.val().trim();
    var old_value = $this.attr('data-value-was');
    if (value == ""){
      $('#users-list-global-roles label.one-name').show();
    }
    else {
      $('#users-list-global-roles label.one-name').hide();
      $('#users-list-global-roles label.one-name:Contains('+value+')').show();
    }
    if (value != old_value){
      $this.attr('data-value-was', value);
      data = { user_name: value };
      RMPlus.GlobalRoles.FetchUsers(this, data);
    }
  }));

});