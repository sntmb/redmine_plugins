$(document).ready(function( ) {
  $('#permissions label.floating input[type="checkbox"]').each(function() {
    var $this = $(this);
    $this.parent().append('<a href="#" data-perm="' + this.value + '" rel="tooltip" class="icon icon-inform no_line">&nbsp;</a>');
  });

  $('#permissions label.floating a.icon').tooltip({ container: 'body', html: true, placement: 'right', title: function() {
    var perm = RMPlus.GlobalRoles.permissions[this.getAttribute('data-perm')];
    if (perm && (perm.actions || perm.desc)) {
      var title = perm.desc ? perm.desc + '<br>' : '';
      if (perm.actions && perm.actions.length > 0) {
        title += '<b>' + RMPlus.GlobalRoles.lbl_routes + ':</b>';
        title += '<ul class="gr-routes">';
        title += '<li>' + perm.actions.join('</li><li>') + '</li>';
        title += '</ul>';
      }
      return title;
    } else { return ''; }
  }});
});